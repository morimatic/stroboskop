# Spletni barvni stroboskop

1\. domača naloga pri predmetu [Osnove informacijskih sistemov](https://ucilnica.fri.uni-lj.si/course/view.php?id=54) (navodila)


## 6.1 Opis naloge in navodila

Na Bitbucket je na voljo javni repozitorij [https://bitbucket.org/asistent/stroboskop](https://bitbucket.org/asistent/stroboskop), ki vsebuje nedelujoč spletni stroboskop. V okviru domače naloge ustvarite kopijo repozitorija ter popravite in dopolnite obstoječo implementacijo spletne strani tako, da bo končna aplikacija z vsemi delujočimi funkcionalnostimi izgledala kot na spodnji sliki. Natančna navodila si preberite na [spletni učilnici](https://ucilnica.fri.uni-lj.si/mod/workshop/view.php?id=19181).

![Stroboskop](stroboskop.gif)

## 6.2 Vzpostavitev repozitorija

Naloga 6.2.2:

```
git clone https://morimatic@bitbucket.org/morimatic/stroboskop.git
```

Naloga 6.2.3:
https://bitbucket.org/morimatic/stroboskop/commits/0586789a3a387ecec268317b312fd16923de90db

## 6.3 Skladnja strani in stilska preobrazba

Naloga 6.3.1:
https://bitbucket.org/morimatic/stroboskop/commits/0873ec1a677a22f2cc3fe055c92f3a8a03fba7ee

Naloga 6.3.2:
https://bitbucket.org/morimatic/stroboskop/commits/69208b4fcba63f89d78be62bc3c954eed231c9b7

Naloga 6.3.3:
https://bitbucket.org/morimatic/stroboskop/commits/5a560a178270a9c448c452388cab4321443a10c1

Naloga 6.3.4:
https://bitbucket.org/morimatic/stroboskop/commits/1719dfccb6e01a8b667843786d678b959eacd999

Naloga 6.3.5:

```
git checkout master
git merge izgled
git push origin master
```

## 6.4 Dinamika in animacija na strani

Naloga 6.4.1:
https://bitbucket.org/morimatic/stroboskop/commits/f78dfa04be605492430931b0d66817c0645b6ad3

Naloga 6.4.2:
https://bitbucket.org/morimatic/stroboskop/commits/0017bc7f52c3cf33345cb80a0db25e4dcab29124

Naloga 6.4.3:
https://bitbucket.org/morimatic/stroboskop/commits/f9e1543833ac0168586952f8e33b31ad34ea7ad7

Naloga 6.4.4:
https://bitbucket.org/morimatic/stroboskop/commits/df8d705b13be1c8aed231ae4be29d9a0f15278f8